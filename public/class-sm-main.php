<?php

/**
 * DPG Media main plugin for global DPG settings
 *
 * @package   SM_Main
 * @author    DPG Media <wordpress_beheer.nl@dpgmediamagazines.nl>
 * @license   GPL-2.0+
 * @link      https://www.dpgmediamagazines.nl
 * @copyright 2015 DPG Media
 */
class SM_Main {

	/**
	 * Plugin version, used for cache-busting of style and script file references.
	 *
	 * @var string
	 */
	const VERSION = '4.0.2';

	/**
	 * Unique identifier for your plugin.
	 *
	 * @var string
	 */
	protected $plugin_slug = 'sm-main';

	/**
	 * Instance of this class.
	 *
	 * @var SM_Main
	 */
	protected static $instance = null;

	/**
	 * Object with the settings of this plugin
	 *
	 * @var stdClass
	 */
	private $plugin_settings;

	/**
	 * Initialize the plugin by setting localization and loading public scripts
	 * and styles.
	 */
	private function __construct() {

		$this->get_plugin_settings();
		// Load plugin text domain
		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );

		add_action('wp_head', array( $this, 'load_ie_warning' ));
    }

	/**
	 * Set widget template
	 *
	 * @param  string $plugin plugin name
	 * @param  string $widget widget name
	 * @param  string $plugin_dir current plugin dir
	 *
	 * @return string Contents of template
	 */
	public static function sm_get_widget_view( $plugin, $widget, $plugin_dir ) {
		$template = locate_template( 'plugins/' . $plugin . '/widget-' . $widget . '.php' );

		if ( empty( $template ) ) {
			$template = trailingslashit( $plugin_dir ) . 'public/views/widget-' . $widget . '.php';
		}

		return $template;
	}

	/**
	 * Return the plugin slug.
	 *
	 * @return string Plugin slug variable.
	 */
	public function get_plugin_slug() {
		return $this->plugin_slug;
	}

    /**
     *
     * @echo string for ie warning.
     */
    public function load_iediv() {
        echo '<div class="ie-warning" style="display:none;"><span class="close-ie-warning"></span><p>Deze website gebruikt technieken die door Internet Explorer niet worden ondersteund. Gebruik een moderne browser: <a href="https://www.google.com/chrome/">Chrome</a> of <a href="https://www.mozilla.org/en-US/firefox/new/">Firefox</a>. <br/><br/> <span style="font-size: 12px;">Beginning January 12, 2016, only the most current version of Internet Explorer available for a supported operating system will receive technical supports and security updates. <b> More information: <a href="https://www.microsoft.com/nl-nl/windowsforbusiness/end-of-ie-support" style="color: black;">https://www.microsoft.com/en-us/windowsforbusiness/end-of-ie-support</a></b></span></p></div>';
    }


    /**
     * Enque css and js for ie warning
     *
     * @enque Js and css for ie warning.
     */
    public function load_ie_warning() {
        if ( isset($this->plugin_settings->iecheckbox_field) && $this->plugin_settings->iecheckbox_field === 'on') {
            // add message inside the footer. hide this by default.
            if ( !is_feed( ) ) {
                add_action('wp_footer', array($this, 'load_iediv'));
            }
            // enque script + styles for the ie warning.
            wp_enqueue_style('ie-message-styling', '-content/plugins/sm-main/public/css/ie-warning.css', array(), $this::VERSION);
            wp_enqueue_script('ie-warning', '-content/plugins/sm-main/public/js/ie-warning-check.js', array('jquery'), $this::VERSION, true);
        }
    }

	/**
	 * Return an instance of this class.
	 *
	 * @return SM_Main A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Load the plugin text domain for translation.
	 */
	public function load_plugin_textdomain() {

		$domain = $this->plugin_slug;
		$locale = apply_filters( 'plugin_locale', get_locale(), $domain );

		load_textdomain( $domain, trailingslashit( WP_LANG_DIR ) . $domain . '/' . $domain . '-' . $locale . '.mo' );
		load_plugin_textdomain( $domain, false, basename( plugin_dir_path( dirname( __FILE__ ) ) ) . '/languages/' );
	}

	/**
	 * get_environment
	 *
	 * Returns the current environment wich is used.
	 * Possible return values: production/staging/test/development
	 *
	 * @return string type of environment
	 */
	public static function get_environment() {
		if ( defined( 'IS_PRODUCTION' ) ) {
			return 'production';
		}

		if ( defined( 'IS_STAGING' ) ) {
			return 'staging';
		}

		if ( defined( 'IS_TEST' ) ) {
			return 'test';
		}

		if ( defined( 'IS_DEVELOPMENT' ) ) {
			return 'development';
		}
	}

	/**
	 * Auto loader
	 *
	 * @param string $class
	 */
	public static function auto_loader( $class ) {
		$auto_load_directories = apply_filters( 'sm_auto_loader', array() );

		foreach ( $auto_load_directories as $namespace => $directory ) {
			if ( 0 === strpos( $class, $namespace ) ) {
				$class = str_replace( $namespace, '', $class );
				$parts = explode( '_', $class );

				$directories = array_map( function ( $part ) {
					return strtolower( $part );
				}, $parts );

				$file = $directory . implode( '/', $directories ) . '.php';

				if ( ( @include( $file ) ) !== false ) {
					break;
				}
			}
		}
	}

	/**
	 * Get the plugin settings from the database
	 *
	 * @return stdClass
	 */
	public function get_plugin_settings() {
		// If the single instance hasn't been set, set it now.
		if ( empty( $this->plugin_settings ) ) {
			$this->plugin_settings = json_decode( json_encode( get_option( $this->plugin_slug ) ) );
		}

		return $this->plugin_settings;
	}

	public static function get_sitecode() {
		if ( function_exists( 'icl_object_id' ) && defined( 'ICL_LANGUAGE_CODE' ) && ICL_LANGUAGE_CODE != "" ) {
			$settings = get_option( 'sm-main' );

			if ( isset( $settings ) && ! empty( $settings ) && ICL_LANGUAGE_CODE != "" ) {
				if ( array_key_exists( 'sitecode_field_' . ICL_LANGUAGE_CODE, $settings ) ) {
					return $settings[ 'sitecode_field_' . ICL_LANGUAGE_CODE ];
				} else {
					return false;
				}

			}

		} else {
			$settings = get_option( 'sm-main' );
			if ( ! empty( $settings ) ) {
				return $settings['sitecode_field'];
			}
		}

		return false;
	}
}

/**
 * Shorthand to get a widget view.
 *
 * @param $plugin_basedir
 * @param $plugin
 * @param $widget
 *
 * @return string
 */
function sm_get_widget_view( $plugin_basedir, $plugin, $widget ) {
	$sm_main = SM_Main::get_instance();

	return $sm_main::sm_get_widget_view( $plugin_basedir, $plugin, $widget );
}
