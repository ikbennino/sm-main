var ua = window.navigator.userAgent;
if (ua.indexOf("Trident/7.0") > -1) {
    jQuery('body').addClass('ie11');
}
if (ua.indexOf("Trident/6.0") > -1) {
    jQuery('body').addClass('ie10');
}
if (ua.indexOf("Trident/5.0") > -1) {
    jQuery('body').addClass('ie9');
}

jQuery(document).ready( function () {
    jQuery('.close-ie-warning').click(function() {
        jQuery('body').removeClass('ie9');
        jQuery('body').removeClass('ie10');
        jQuery('body').removeClass('ie11');
    });
});
