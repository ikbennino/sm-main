<?php
/**
 * DPG Media main plugin for global DPG settings
 *
 * @package   SM_Main
 * @author    DPG Media <wordpress_beheer.nl@dpgmediamagazines.nl>
 * @license   GPL-2.0+
 * @link      https://www.dpgmediamagazines.nl
 * @copyright 2015 DPG Media
 */
?>
<div class="wrap">
    <h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
    <form method="POST" action="options.php">
        <?php
        settings_fields( 'sm-main' );
        do_settings_sections( 'sm-main' );
        submit_button();
        ?>
    </form>
</div>
