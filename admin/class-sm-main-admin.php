<?php

/**
 * DPG Media main plugin for global DPG settings
 *
 * @package   SM_Main
 * @author    DPG Media <wordpress_beheer.nl@dpgmediamagazines.nl>
 * @license   GPL-2.0+
 * @link      https://www.dpgmediamagazines.nl
 * @copyright 2015 DPG Media
 */
class SM_Main_Admin {

    /**
     * Instance of this class.
     *
     * @var SM_Main_Admin
     */
    protected static $instance = null;

    /**
     * Slug of the plugin screen.
     *
     * @var string
     */
    protected $plugin_screen_hook_suffix = null;

    /**
     * Instance of the frontend plugin object
     *
     * @var SM_Main
     */
    private $plugin = null;

    /**
     * Shorthand holder for the plugin slug
     *
     * @var string
     */
    private $plugin_slug = null;

    /**
     * Initialize the plugin by loading admin scripts & styles and adding a
     * settings page and menu.
     */
    private function __construct() {

        // Set private vars
        $this->plugin      = SM_Main::get_instance();
        $this->plugin_slug = $this->plugin->get_plugin_slug();

        // Add the options page and menu item.
        add_action( 'admin_menu', array( $this, 'add_plugin_admin_menu' ), 1 );
        add_action( 'admin_init', array( $this, 'add_settings_sections' ) );
        add_action( 'admin_init', array( $this, 'add_settings_fields' ) );
        add_action( 'admin_init', array( $this, 'register_settings' ), 20 );
    }

    /**
     * Return an instance of this class.
     *
     * @return    SM_Main_Admin    A single instance of this class.
     */
    public static function get_instance() {

        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Register settings of this plugin
     */
    public function register_settings() {
        register_setting( $this->plugin_slug, $this->plugin_slug );
    }

    /**
     * add new settings to WordPress DB
     *
     * @codeCoverageIgnore
     * @return null
     */
    public function add_settings_sections() {

        add_settings_section(
            'sm-main' . '-general-settings',
            __( 'General Settings', 'sm-main' ),
            false,
            'sm-main'
        );
        $this->plugin_settings = get_option('sm-main');


    }

    /**
     * Defines settings fields for registered available settings
     *
     * @codeCoverageIgnore
     * @return null
     */
    public function add_settings_fields() {
    	//check if wpml is active
        if ( function_exists( 'icl_object_id' ) ) {
            //get active wpml languages
            $active_languages = apply_filters( 'wpml_active_languages', NULL );
            if ( ! empty( $active_languages ) ) {
                foreach ( $active_languages as $active_language ) {
                    if ( isset( $active_language['code'] ) ) {
                        $code = $active_language['code'];
                    } elseif ( isset($active_language['language_code'] ) ) {
                        $code = $active_language['language_code'];
                    } else {
                        continue;
                    }

                    add_settings_field(
                        'sm-main--sitecode__field-' . $code,
                        __( 'SITECODE for ' . $active_language['native_name'] .'(' . $code . ')' , 'sm-main' ),
                        array( $this, 'language_sitecode_field' ),
                        'sm-main',
                        'sm-main' . '-general-settings',
                        array(
                            'label_for' => 'sitecode_field_' . $code,
                            'language_code'  => $code
                        )
                    );
                }
            }
        } else {
            add_settings_field(
                'sm-main--sitecode_field',
                __( 'SITECODE', 'sm-main' ),
                array( $this, 'main_sitecode_field' ),
                'sm-main',
                'sm-main' . '-general-settings',
                array( 'label_for' => 'sitecode_field' )
            );
            add_settings_field(
                'sm-main--iewarning_checkbox',
                __( 'Suggest different browser than internet explorer', 'sm-main' ),
                array( $this, 'main_iewarning_checkbox' ),
                'sm-main',
                'sm-main' . '-general-settings',
                array( 'label_for' => 'iecheckbox_field' )
            );
        }
    }

    /**
         * Field to set sitecode (only necessary for Belgian sites)
         *
         * @codeCoverageIgnore
         *
         * @param  array     $args name value pair for fields
         * @return buffer     prints field to output buffer
         */
    public function main_sitecode_field( $args ) {
	    echo '<input type="text" name="' . 'sm-main' . '[sitecode_field]" id="' . $args['label_for'] . '"' . ( isset( $this->plugin_settings['sitecode_field'] ) && ! empty( $this->plugin_settings['sitecode_field'] ) ? ' value="' . $this->plugin_settings['sitecode_field'] . '"' : '' ) . ' />';
    }

    public function main_iewarning_checkbox( $args ) {
        echo '<input type="checkbox" name="' . 'sm-main' . '[iecheckbox_field]" id="' . $args['label_for'] . '"' . ( isset( $this->plugin_settings['iecheckbox_field'] ) && ! empty( $this->plugin_settings['iecheckbox_field'] ) ? ' checked="' . $this->plugin_settings['iecheckbox_field'] . '"' : '' ) . ' />';
    }

    public function language_sitecode_field($args) {
	    echo '<input type="text" name="' . 'sm-main[sitecode_field_'. $args['language_code'] .']" id="' . $args['label_for'] .
	         '"' . ( isset( $this->plugin_settings['sitecode_field_' . $args['language_code']] ) &&
	                 ! empty( $this->plugin_settings['sitecode_field_' . $args['language_code']] ) ?
			    ' value="' . $this->plugin_settings['sitecode_field_' . $args['language_code']] . '"' :
			    '' ) . ' />';
    }

    /**
     * Register the administration menu for this plugin into the WordPress Dashboard menu.
     */
    public function add_plugin_admin_menu() {
        $this->plugin_screen_hook_suffix = add_menu_page(
            __( 'DPG Media', $this->plugin_slug ),
            __( 'DPG Media', $this->plugin_slug ),
            'manage_options',
            $this->plugin_slug,
            array( $this, 'display_plugin_admin_page' )
        );
    }

    /**
     * Render the settings page for this plugin.
     */
    public function display_plugin_admin_page() {
        include_once( 'views/admin.php' );
    }
}
