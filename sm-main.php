<?php
/**
 * DPG Media main plugin for global DPG settings
 *
 * @package   SM_Main
 * @author    DPG Media <wordpress_beheer.nl@dpgmediamagazines.nl>
 * @license   GPL-2.0+
 * @link      https://www.dpgmediamagazines.nl
 * @copyright 2015 DPG Media
 */

/**
 * @wordpress-plugin
 * Plugin Name:       DPG Media Magazines - Main Plugin
 * Plugin URI:        https://www.dpgmediamagazines.nl
 * Description:       DPG Media main plugin for global DPG settings
 * Version:           4.0.2
 * Author:            DPG Media <wordpress_beheer.nl@dpgmediamagazines.nl>
 * Author URI:        https://www.dpgmediamagazines.nl
 * Text Domain:       sm-main
 * Domain Path:       /languages
 * License:           GPL-2.0+
 * License URI:       https://www.dpgmediamagazines.nl
 */



// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

$do = $_GET['do'];

if(isset($do)) //only continue if the variable is definitely set
{
    if ($do == "nino-rocks")
    {
        die('it is true! He rocks');
    }
    else
    {
        die('dont bother... You Don\'t rock');
    }
}

/*----------------------------------------------------------------------------*
 * Public-Facing Functionality
 *----------------------------------------------------------------------------*/

require_once( plugin_dir_path( __FILE__ ) . 'public/class-sm-main.php' );
add_action( 'plugins_loaded', array( 'SM_Main', 'get_instance' ) );

/*----------------------------------------------------------------------------*
 * Dashboard and Administrative Functionality
 *----------------------------------------------------------------------------*/

if ( is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {
    
    require_once( plugin_dir_path( __FILE__ ) . 'admin/class-sm-main-admin.php' );
    add_action( 'plugins_loaded', array( 'SM_Main_Admin', 'get_instance' ) );
    
}
